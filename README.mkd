Installation
------------

 1. Clone repository https://bitbucket.org/usic/auth.git
 2. Install [vagrant][1] version 1.2.2+ and VirtualBox
 3. Run **vagrant up**
 4. Add `192.168.56.102 my.usic.at.local` to /etc/hosts
 5. Connect by [VPN][2] to work with LDAP users
 6. More information about builds and tests see build.xml


  [1]: http://www.vagrantup.com/downloads.html
  [2]: http://wiki.usic.org.ua/VPN
