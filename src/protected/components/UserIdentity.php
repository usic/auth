<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		if (LdapUtils::userCheckPassword(
				array(
					'login'    => $this->username,
					'password' => $this->password
				)
			) !== true
		) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->errorCode = self::ERROR_NONE;
			$model = FastUser::model()->findOne(array('login' => $this->username));
			if (is_null($model)) {
				$userData = LdapUtils::userInfo(
					array(
						'login' => $this->username
					)
				);
				$userData['groups'] = LdapUtils::userShowGroups($this->username);
			} else {
				$userData = $model->getDocument();
                unset($userData['_id']);
			}
			foreach ($userData as $k => $v) {
				$this->setState($k, $v);
			}
		}
		return !$this->errorCode;
	}
}
