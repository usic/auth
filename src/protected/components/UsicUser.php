<?php

class UsicUser extends CWebUser
{

	public $identityCookie = array(
		'path'     => '/',
		'httpOnly' => true
	);

	public function init()
	{
		$this->identityCookie['domain'] = ((PHP_SAPI != 'cli') ? substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], '.')) : false);
		$this->autoRenewCookie = true;
		$this->setStateKeyPrefix('');
		parent::init();
	}

	public function getIsGuest()
	{
		return $this->getState('id') === null;
	}


	public function getId()
	{
		return $this->getState('id');
	}


	public function setId($value)
	{
		$this->setState('id', $value);
	}

	public function getName()
	{
		if (($name = $this->getState('name')) !== null) {
			return $name;
		} else {
			return $this->guestName;
		}
	}

	public function setName($value)
	{
		$this->setState('name', $value);
	}

	public function login($identity, $duration = 0)
	{
		$id = $identity->getId();
		$sesssion = Yii::app()->getSession();
		$states = $identity->getPersistentStates();
		if ($this->beforeLogin($id, $states, false)) {
			if ($duration > 0) {
				$sesssion->setTimeout($duration);
				if ($this->allowAutoLogin) {
					$sesssion->setCookieParams(array('lifetime' => $duration));
				} else {
					throw new CException(Yii::t('yii', '{class}.allowAutoLogin must be set true in order to use cookie-based authentication.',
						array('{class}' => get_class($this))));
				}
			}
			$this->changeIdentity($id, $identity->getName(), $states);
			if ($this->absoluteAuthTimeout) {
				$this->setState(self::AUTH_ABSOLUTE_TIMEOUT_VAR, time() + $this->absoluteAuthTimeout);
			}
			$this->afterLogin(false);
		}
		return !$this->getIsGuest();
	}

	public function inGroup($group)
	{
		return !$this->getIsGuest() && in_array($group, $this->groups);
	}
}

?>