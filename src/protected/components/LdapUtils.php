<?php

class LdapUtils
{
	private static $_directory;
	const DEFAULT_GROUP = 'users';

	private static function _directory()
	{
		if (empty(self::$_directory)) {
			self::$_directory = Yii::getPathOfAlias('application') . '/../ldap-utils/src/';
		}
		return self::$_directory;
	}

	public static function userCheckPassword($arg)
	{
		$command = 'echo "' . $arg["password"] . '" | ' . self::_directory() . 'usiccheckpasswd ' . $arg["login"];
		exec($command, $values, $result);
		return self::processAuthErrors($result);
	}

	protected static function processAuthErrors($error)
	{
		switch ($error) {
			case 0:
				return true;
			case 11:
				$error = [
					'status'  => $error,
					'message' => 'Логін або пароль некоректний'
				];
				break;
			case 15:
				$error = [
					'status'  => $error,
					'message' => 'Зєднання з сервером авторизації невдале',
				];
				break;
			case 111:
				$error = [
					'status'  => $error,
					'message' => 'Сервер авторизації не відповідає'
				];
				break;
			case 126:
				$error = [
					'status'  => $error,
					'message' => 'Невірно встановлені права на скрипти'
				];
				break;
			default:
				$error = [
					'status'  => $error,
					'message' => 'Нівідома помилка сервера авторизації'
				];
				break;
		}
		Yii::log(sprintf("Error code:%s. Error message: %s", $error['status'], $error['message']), 'error', 'ldap.auth');
		return $error;
	}

	public static function userAdd($arg)
	{
		$values = array();
		$result = 0;
		$fields = self::$_ldapFields;
		unset($fields['name']);
		unset($fields['login']);
#        unset($fields['phone']);
		$fields['password'] = 'password';
		$fields['class'] = 'level';
		$command = "echo -e \"login=" . $arg['login'];
		if (!empty($arg['name'])) {
			$command
				.= "\\n name=" . $arg['surname'] . " " . $arg['name'] . " " . $arg['middleName'];
		}
		foreach ($fields as $k => $v) {
			if (isset($arg[$v])) {
				$command .= "\\n " . $k . "=" . $arg[$v];
			}
		}
		$command .= "\" | " . self::_directory() . "usic_useradd";
		exec($command, $values, $result);
		return self::processUserErrors($result);
	}

	public static function userSearch($arg)
	{
		$values = array();
		$result = 0;
		$command = "echo -e \"";
		if (!is_null($arg['surname'])) {
			$command
				.= "name=" . $arg['surname'] . " " . $arg['name'] . " " . $arg['middleName'] . "\\n";
		}
		if (!is_null($arg['password'])) {
			$command .= "password=" . $arg['password'] . "\\n";
		}
		if (!is_null($arg['enteringYear'])) {
			$command .= "entry_year=" . $arg['enteringYear'] . "\\n";
		}
		if (!is_null($arg['profession'])) {
			$command .= " profession=" . $arg['profession'] . "\\n";
		}
		if (!is_null($arg['status'])) {
			$command .= "class=" . $arg['status'] . "\\n";
		}
		if (!is_null($arg['login'])) {
			$command .= " login=" . $arg['login'] . "\\n";
		}
		echo $command .= "\" | " . self::_directory() . "usic_search.pl";
		exec($command, $values, $result);
		return $values;
	}

	public static function userMod($arg)
	{
		$values = array();
		$result = 0;
		$fields = self::$_ldapFields;
		unset($fields['name']); // not simple change
		unset($fields['login']); //can not be changed
		$fields['password'] = 'password'; // password not display, so field to change it is added here
		$fields['class'] = 'level';
		$command = "echo  -e \"login=" . $arg['login'];
		if (!empty($arg['name'])) {
			$command
				.= "\\n name=" . $arg['surname'] . " " . $arg['name'] . " " . $arg['middleName'];
		}
		foreach ($fields as $k => $v) {
			if (!empty($arg[$k])) {
				$command .= "\\n " . $v . "=" . $arg[$k];
			}
		}

		$command .= "\" | " . self::_directory() . "usic_usermod";
		exec($command, $values, $result);
		return self::processUserErrors($result);
	}

	/**
	 * @param $args
	 *
	 * @return array
	 */
	public static function userInfo($args)
	{
		$values = array();
		$result = 0;
		$command = "echo -e \"login=" . $args['login'] . "\\n values=" .
			(!isset($args['values'])
				? implode(",", array_keys(self::$_ldapFields))
				: implode(',', $args['values'])) . "\" | " . self::_directory() . "usic_userinfo";
		exec($command, $values, $result);
		$error = self::processUserErrors($result);
		if ($error) {
			return self::_constructUserArray($values);
		} else {
			return $error;
		}
	}

	/**
	 * @param $login
	 *
	 * @return array
	 */
	public static function userDel($login)
	{
		$values = array();
		$result = 0;
		$command = "echo " . $login . " | " . self::_directory() . "usic_userdel";
		exec($command, $values, $result);
		return self::processUserErrors($result);
	}

	/**
	 * @param $login
	 *
	 * @return array
	 */
	public static function userShowGroups($login)
	{
		$userGroups = array();
		$groups = self::groupShowUsers();
		if ($groups) {
			foreach ($groups as $g) {
				if (self::groupCheckUser(array('login' => $login, 'group' => $g))) {
					$userGroups[] = $g;
				}
			}
		}
		return $userGroups;
	}

	/**
	 * @param $args
	 *
	 * @return array|bool
	 */
	public static function groupCheckUser($args)
	{
		$values = array();
		$result = 0;
		$command = self::_directory() . "usicgroup check " . $args['group'] . " " . $args['login'];
		exec($command, $values, $result);
		return self::processGroupErrors($result) === true; // we need get strong true or false here. See userShowGroups
	}

	/**
	 * @param $error
	 *
	 * @return array
	 */
	protected static function processUserErrors($error)
	{
		switch ($error) {
			case 0:
				return true;
				break;
			case 15:
				$error = [
					'status'  => $error,
					'message' => 'LDAP відхилив запит'
				];
				break;
			case 22:
				$error = [
					'status'  => $error,
					'message' => 'помилка передачі параметрів (конфігураційним файлом або на stdin)'
				];
				break;
			case 32:
				$error = [
					'status'  => $error,
					'message' => 'Нічого не знайдено'
				];
				break;
			case 33:
				$error = [
					'status'  => $error,
					'message' => 'Oбліковий запис за заданими параметрами вже існує в базі'
				];
				break;
			case 111:
				$error = [
					'status'  => $error,
					'message' => 'Не можу з’єднатись з LDAP’ом'
				];
				break;
			case 126:
				$error = [
					'status'  => $error,
					'message' => 'Невірно встановлені права на скрипти'
				];
				break;
			default:
				$error = [
					'status'  => $error,
					'message' => 'Невідома помилка'
				];
				break;
		}
		Yii::log(sprintf("Error code:%s. Error message: %s", $error['status'], $error['message']), 'error', 'ldap.user');
		return false;
	}

	### BEGIN GROUPS ###
	/**
	 * @param $name
	 *
	 * @return bool
	 */
	public static function groupAdd($name)
	{
		$values = array();
		$result = 0;
		$command = self::_directory() . "usicgroup add " . $name;
		exec($command, $values, $result);
		return self::processGroupErrors($result);
	}

	public static function groupAddUser($args)
	{
		$values = array();
		$result = 0;
		$login = (isset($args['login'])) ? ((is_array($args['login'])) ? implode(" ", $args['login']) : $args['login'])
			: null;
		$command = self::_directory() . "usicgroup add " . $args['group'] . " " . $login;
		exec($command, $values, $result);
		return self::processGroupErrors($result);
	}


	/**
	 * @param null $group
	 *
	 * @return array|bool
	 */
	public static function groupShowUsers($group = null)
	{
		$values = array();
		$result = 0;
		$flag = false;
		$c = 0;
		if (is_array($group)) {
			$c = count($group);
			$group = implode(" ", $group);
		}
		$command = self::_directory() . "usicgroup show " . $group;
		exec($command, $values, $result);
		if (self::processGroupErrors($result) === true) {
			foreach ($values as $k => $v) {
				$values[$k] = trim($v);
				if (preg_match('/^(.*):/', $v)) { // hack to prevent display group names
					unset($values[$k]);
				}
			}
			asort($values);
			return array_values(array_unique($values));
		}
		return self::processGroupErrors($result);
	}

	//remove user from group or full group
	public static function groupRemove($args)
	{
		$values = array();
		$result = 0;
		$login = (isset($args['login'])) ? ((is_array($args['login'])) ? implode(" ", $args['login']) : $args['login'])
			: null;
		$command = self::_directory() . "usicgroup remove " . $args['group'] . " " . $login;
		exec($command, $values, $result);
		return self::processGroupErrors($result);
	}

	protected static function processGroupErrors($error)
	{
		switch ($error) {
			case 0:
				return true;
			case 15:
				$error = [
					'status'  => $error,
					'message' => 'LDAP відхилив запит'
				];
				break;
			case 33:
				$error = [
					'status'  => $error,
					'message' => 'Група вже існує'
				];
				break;
			case 111:
				$error = [
					'status'  => $error,
					'message' => 'Відсутнє з\'єднання з сервером'
				];
				break;
			default:
				$error = [
					'status'  => $error,
					'message' => 'Невідома помилка'
				];
				break;
		}
		Yii::log(sprintf("Error code: %s. Error message: %s", $error['status'], $error['message']), 'error', 'ldap.group');
		return false;
	}

	static private $_ldapFields
		= array(
			'login'      => 'login',
			'name'       => 'name',
			'email'      => 'email',
			'phone'      => 'phone',
			'faculty'    => 'faculty',
			'profession' => 'profession',
			'entry_year' => 'enteringYear',
		);

	private static function _constructUserArray($values)
	{
		$arr = array();
		foreach ($values as $value) {
			$str = explode("=", $value);
			$arr[self::$_ldapFields[$str[0]]] = $str[1];
		}
		if (in_array('name', array_keys($arr))) {
			$nameValues = array(0 => 'surname', 1 => 'name', 2 => 'middleName');
			$a = explode(" ", $arr['name']);
			foreach ($nameValues as $k => $v) {
				$arr[$v] = isset($a[$k]) ? $a[$k] : null;
			}
		}
		return $arr;
	}

}
