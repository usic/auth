<?php

/**
 *   SlowUser - model to store user data in LDAP.
 *
 *    Do not use to directly add/change user`s data in LDAP, use syncToSlowModel() function from FastUser model instead
 */
class SlowUser extends LdapUser
{
	public $login;
	public $password;
	public $name;
	public $surname;
	public $middleName;
	//public $faculty;
	public $profession; // profession full
	public $level;
	public $phone;
	public $email;
	public $enteringYear;

	public $newPassword;
	public $newEmail;
	public $newPhone;

	const LEVEL_BACHELOR = 0,
		LEVEL_SPECIAL = 1,
		LEVEL_MASTER = 2,
		LEVEL_TEACHER = 3,
		LEVEL_EMPLOYER = 4;

	public function __construct()
	{
		$this->setIsNewRecord(true);
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('login,password,name,surname,middleName,profession,level,phone,email,enteringYear', 'safe', 'on' => 'test'),
		);
	}

	public function attributeNames()
	{
		return array(
			'login', 'password', 'name', 'surname', 'middleName', 'profession', 'level', 'phone', 'email',
			'enteringYear'
		);
	}

	public function attributeLabels()
	{
		return array(
			'login'        => 'Логін',
			'password'     => 'Пароль',
			'name'         => 'Ім\'я',
			'surname'      => 'Прізвище',
			'middleName'   => 'По-батькові',
			'profession'   => 'Спеціальність',
			'level'        => 'Рівень',
			'email'        => 'Email',
			'phone'        => 'Телефон',
			'enteringYear' => 'Рік вступу',
			'pasCode'      => 'Номер паспорта'
		);
	}

	/**
	 *    Returns user`s full name
	 * @return string
	 *
	 */
	public function getFullName()
	{
		return $this->surname . " " . $this->name . " " . $this->middleName;
	}

	public function auth($login, $password)
	{
		Yii::trace(get_class($this) . '.auth()');
		return LdapUtils::userCheckPassword(
			array(
				'login'    => $login,
				'password' => $password
			)
		);
	}

	/**
	 *    Returns level const based on string representation
	 *
	 * @param string $levelString String level represantation
	 * @return int level const. Null if no such level
	 *
	 */
	public static function levelConst($levelString)
	{
		switch ($levelString) {
			case "Бакалавр":
				return self::LEVEL_BACHELOR;
				break;
			case "Магістр" :
				return self::LEVEL_MASTER;
				break;
			case "Спеціаліст":
				return self::LEVEL_SPECIAL;
				break;
			default:
				return null;
				break;
		}
	}

	protected function beforeSave()
	{
		foreach(['surname','name','middleName'] as $item){
			$this->$item = str_replace(["`",""],["’"], $this->$item);
		}
		if (is_string($this->level)) {
			$this->level = self::levelConst($this->level);
		}
		if (preg_match('/\D+/', $this->profession)) {
			$this->profession = '11111';
		}
		if (!isset($this->enteringYear)) {
			$this->enteringYear = '1901';
		}
		if (!isset($this->phone)) {
			$this->phone = '0000000000';
		}
		return parent::beforeSave();
	}

}