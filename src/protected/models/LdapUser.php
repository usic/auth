<?php

class LdapUser extends CModel
{
	private $_new;

	/**
	 * @param string $className
	 *
	 * @return mixed
	 */
	public static function model($className = __CLASS__)
	{
		return new $className;
	}

	/* NO NOT REMOVE THIS METHOD */
	public function attributeNames()
	{
		return parent::attributeNames();
	}

	public function onBeforeSave($event)
	{
		$this->raiseEvent('onAfterSave', $event);
	}

	public function onAfterSave($event)
	{
		$this->raiseEvent('onAfterSave', $event);
	}

	public function onAfterFind($event)
	{
		$this->raiseEvent('onAfterFind', $event);
	}

	public function onBeforeFind($event)
	{
		$this->raiseEvent('onBeforeFind', $event);
	}

	protected function beforeSave()
	{
		if ($this->hasEventHandler('onBeforeSave')) {
			$event = new CModelEvent($this);
			$this->onBeforeSave($event);
			return $event->isValid;
		} else {
			return true;
		}
	}

	protected function afterSave()
	{
		if ($this->hasEventHandler('onAfterSave')) {
			$this->onAfterSave(new CEvent($this));
		}
	}

	protected function beforeFind()
	{
		if ($this->hasEventHandler('onBeforeFind')) {
			$event = new CModelEvent($this);
			$this->onBeforeFind($event);
			return $event->isValid;
		} else {
			return true;
		}
	}

	protected function afterFind()
	{
		if ($this->hasEventHandler('onAfterFind')) {
			$this->onAfterFind(new CEvent($this));
		}
	}

	/**
	 * @param $id
	 *
	 * @return bool|User
	 */
	public function findById($id, $afterFind = true)
	{
		if (!$this->beforeFind()) {
			return false;
		}

		$userInfo = LdapUtils::userInfo(array('login' => $id));

		if ($userInfo === false) {
			return false;
		} else {
			$user = new SlowUser;
			foreach ($userInfo as $field => $value) {
				$user->$field = $value;
			}
			$user->setIsNewRecord(false);
			$user->afterFind();
			return $user;
		}
	}

	/**
	 * @param $group
	 *
	 * @return array|bool
	 */
	public function findAllByGroup($group)
	{
		Yii::trace(get_class($this) . '.findAllByGroup()');
		$userNames = LdapUtils::groupShowUsers($group);
		if ($userNames === false) {
			return false;
		}
		$users = array();
		foreach ($userNames as $name) {
			$user = $this->findById($name);
			$users[] = $user;
		}
		return $users;
	}

	/**
	 * @param bool $withData
	 *
	 * @return array|bool
	 */
	public function findAll()
	{
		Yii::trace(get_class($this) . '.findAll()');
		return $this->findAllByGroup(LdapUtils::DEFAULT_GROUP);
	}

	/**
	 * @param bool $runValidation
	 * @param null $attributes
	 *
	 * @return bool|void
	 */

	public function save($runValidation = true, $attributes = null)
	{
		if (!$runValidation || $this->validate($attributes)) {
			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
		} else {
			return false;
		}
	}

	/**
	 * @param array $attributes
	 *
	 * @return bool
	 */
	public function insert(array $attributes = null)
	{
		if (!$this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'User cannot be added to LDAP because he/she is not new.'));
		}
		if (!$this->beforeSave()) {
			return false;
		}
		$args = $this->getAttributes($attributes);
		$result = LdapUtils::userAdd($args);
		if ($result !== true) {
			return false;
		}
		$this->setIsNewRecord(false);
		$this->afterSave();
		return true;
	}

	public function update(array $attributes = null)
	{
		if ($this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'User cannot be upadted because he/she is new.'));
		}
		if (!$this->beforeSave()) {
			return false;
		}
		$args = $this->getAttributes($attributes);
		$result = LdapUtils::userMod($args);
		if ($result !== true) {
			return false;
		}
		$this->afterSave();
		return true;
	}

	//Removes user from users (default) group
	public function delete()
	{
		return $this->removeFromGroup(LdapUtils::DEFAULT_GROUP);
	}

	public function fullDeleteById($id)
	{
		return LdapUtils::userDel($id);
	}

	public function fullDelete()
	{
		if ($this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'User cannot be deleted from LDAP because he/she is new.'));
		}
		return $this->fullDeleteById($this->login);
	}

	public function getGroups()
	{
		if ($this->getIsNewRecord()) {
			return false;
		}
		$result = LdapUtils::userShowGroups($this->login);
		if ($result === false) {
			return false;
		}
		return $result;
	}

	//CHECK
	public function addToGroup($group)
	{
		if ($this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'User cannot be added to group because he/she is new.'));
		}
		//TODO Check if group exists
		$args = array('login' => $this->login, 'group' => $group);
		if (LdapUtils::groupCheckUser($args)) {
			return true;
		}
		$result = LdapUtils::groupAddUser($args);
		if ($result === true) {
			return true;
		} else {
			return false;
		}
	}

	public function removeFromGroup($group)
	{
		if ($this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'User cannot be removed from group because he/she is new.'));
		}
		$args = array('login' => $this->login, 'group' => $group);
		if (!LdapUtils::groupCheckUser($args)) {
			return true;
		}
		$result = LdapUtils::groupRemove($args);
		if ($result === true) {
			return true;
		} else {
			return false;
		}
	}

	public function getIsNewRecord()
	{
		return $this->_new;
	}

	public function setIsNewRecord($value)
	{
		$this->_new = $value;
	}
}