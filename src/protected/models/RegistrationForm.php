<?php

/**
 *    RegistrationForm - data structure to use for registration.
 *    Used by actionRegister from SiteController
 *
 */
class RegistrationForm extends CFormModel
{
	/* New values */
	public $login;
	public $password;
	public $email;

	/* Values to validate with icc data */
	public $surName;
	public $pasCode;

	/* FastUser model */
	public $fastUser;

	public function rules()
	{
		return array(

			array('login,password,email,surName,pasCode', 'required', 'message' => Yii::t('user', 'Поле {attribute} обов’язкове')),

			array('login', 'match', 'pattern' => '/^([a-z0-9_]{5,20})$/', 'message' => Yii::t('user', 'Некоректний формат логіну')),

			array('password', 'length', 'min' => '6', 'tooShort' => Yii::t('user', 'Пароль занадто короткий')),

			array('email', 'email', 'checkMX' => true, 'message' => Yii::t('user', 'Введіть існуючу пошту')),

			array('email', 'newEmail'),

			array('login', 'newLogin'),

			array('pasCode', 'checkPasCode')

		);
	}

	public function attributeNames()
	{
		return array('login', 'password', 'email', 'surName', 'pasCode');
	}

	public function attributeLabels()
	{
		return array(
			'login'    => 'Логін',
			'password' => 'Пароль',
			'email'    => 'Email',
			'surName'  => 'Прізвище',
			'pasCode'  => 'Код перевірки'
		);
	}

	public function newEmail($attribute, $params)
	{
		if (FastUser::model()->exists(array($attribute => $this->$attribute))) {
			$this->addError($attribute, Yii::t('error', 'Користувач з таким email вже існує'));
		}
	}

	/**
	 *   Checks login unicity
	 *
	 */
	public function newLogin($attribute, $params)
	{
		if (FastUser::model()->exists(array($attribute => $this->$attribute))) {
			$this->addError($attribute, Yii::t('error', 'Користувач з таким логіном вже існує'));
		}
	}

	/**
	 *   Checks pasCode and surName with data from icc
	 *
	 */
	public function checkPasCode($attribute, $params)
	{
		$user = FastUser::model()->findOne(array('pasCode' => $this->$attribute, 'surName' => $this->surName));
		if (is_null($user)) {
			$this->addError($attribute, Yii::t('error', 'Особа з таким прізвищем та номером паспорта не є студентом НаУКМА'));
		} elseif (isset($user->login)) {
			$this->addError($attribute, Yii::t('error', 'Користувач з таким прізвищем та номером паспорта вже зареєстрований'));
		}
	}

	/**
	 *    Saves user to FastUser model DB
	 *
	 */
	public function saveToDB()
	{

		if (!$this->validate()) {
			return false;
		}
		$this->fastUser = FastUser::model()->findOne(array('pasCode' => $this->pasCode, 'surName' => $this->surName));

		if (!is_null($this->fastUser)) {
			$this->fastUser->login = $this->login;
			$this->fastUser->email = $this->email;
			array_push($this->fastUser->groups, 'users');
			return $this->fastUser->save();
		} else {
			return false;
		}
	}

	protected function beforeValidate()
	{
		$this->surName = self::_mbUcfirst($this->surName);
		$this->pasCode = self::_mbUcfirst($this->pasCode);
		return parent::beforeValidate();
	}

	/**
	 *    Turns first letter uppercase
	 *
	 */
	private static function _mbUcfirst($str, $encoding = "UTF-8", $lowerStrEnd = true)
	{
		$firstLetter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
		$strEnd = "";
		if ($lowerStrEnd) {
			$strEnd = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
		} else {
			$strEnd = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
		}
		$str = $firstLetter . $strEnd;

		$from = array('A', 'B', 'C', 'E', 'H', 'I', 'K', 'O', 'P', 'T', 'X', 'Y');
		$to = array('А', 'В', 'С', 'Е', 'Н', 'І', 'К', 'О', 'Р', 'Т', 'Х', 'У');
		$str = str_replace($from, $to, $str);

		return $str;
	}
}

