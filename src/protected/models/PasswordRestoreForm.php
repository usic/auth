<?php


class PasswordRestoreForm extends CFormModel
{
	public $email;

	public $password;
	public $repeatPassword;

	private $_fastUser;

	public function rules()
	{
		return array(
			array('email', 'required', 'on' => 'enterEmail'),
			array('email', 'emailExists', 'on' => 'enterEmail'),

			array('password,repeatPassword', 'required', 'on' => 'enterPassword'),
			array('password', 'length', 'min' => '6', 'tooShort' => 'Пароль має бути довший за 6 символів', 'on' => 'enterPassword'),
			array('repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Паролі не співпадають', 'on' => 'enterPassword')
		);
	}

	public function attributeLabels()
	{
		return array(
			'email'          => 'Email',
			'password'       => 'Пароль',
			'repeatPassword' => 'Пароль ще раз'
		);
	}

	public function getFastUser()
	{
		return $this->_fastUser;
	}

	public function emailExists($attribute, $params)
	{
		$this->_fastUser = FastUser::model()->findOne(array($attribute => $this->$attribute));
		if (is_null($this->_fastUser)) {
			$this->addError($attribute, Yii::t('error', 'Не існує користувача з таким email'));
		} elseif (!in_array('confirmed', $this->_fastUser->groups)) {
			$this->addError($attribute, Yii::t('error', 'Ваш email не підтверджений'));
		}
	}
}

