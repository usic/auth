<?php

class LdapGroup extends CModel
{
	private $_new;

	/**
	 * @param string $className
	 *
	 * @return mixed
	 */
	public static function model($className = __CLASS__)
	{
		return new $className;
	}

	/* NO NOT REMOVE THIS METHOD */
	public function attributeNames()
	{
		return parent::attributeNames();
	}

	public function onBeforeSave($event)
	{
		$this->raiseEvent('onAfterSave', $event);
	}

	public function onAfterSave($event)
	{
		$this->raiseEvent('onAfterSave', $event);
	}

	public function onAfterFind($event)
	{
		$this->raiseEvent('onAfterFind', $event);
	}

	public function onBeforeFind($event)
	{
		$this->raiseEvent('onBeforeFind', $event);
	}

	protected function beforeSave()
	{
		if ($this->hasEventHandler('onBeforeSave')) {
			$event = new CModelEvent($this);
			$this->onBeforeSave($event);
			return $event->isValid;
		} else {
			return true;
		}
	}

	protected function afterSave()
	{
		if ($this->hasEventHandler('onAfterSave')) {
			$this->onAfterSave(new CEvent($this));
		}
	}

	protected function beforeFind()
	{
		if ($this->hasEventHandler('onBeforeFind')) {
			$event = new CModelEvent($this);
			$this->onBeforeFind($event);
			return $event->isValid;
		} else {
			return true;
		}
	}

	protected function afterFind()
	{
		if ($this->hasEventHandler('onAfterFind')) {
			$this->onAfterFind(new CEvent($this));
		}
	}

	public function findById($id)
	{
		if (!$this->beforeFind()) {
			return false;
		}
		$groups = LdapUtils::groupShowUsers();
		if ($groups === false) {
			return false;
		}
		if (in_array($id, $groups)) {
			$group = new Group;
			$group->name = $id;
			$group->setIsNewRecord(false);
			$group->afterFind();
			return $group;
		} else {
			return false;
		}
	}

	public function findAll()
	{
		Yii::trace(get_class($this) . '.findAll()');
		if (!$this->beforeFind()) {
			return false;
		}
		$groupNames = LdapUtils::groupShowUsers();
		if ($groupNames === false) {
			return false;
		}
		$groups = array();
		foreach ($groupNames as $name) {
			$group = new Group;
			$group->name = $name;
			$group->setIsNewRecord(false);
			$group->afterFind();
			$groups[] = $group;
		}
		return $groups;
	}

	public function save($runValidation = true, $attributes = null)
	{
		if (!$runValidation || $this->validate($attributes)) {
			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
		} else {
			return false;
		}
	}

	public function insert(array $attributes = null)
	{
		if (!$this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'Group cannot be added to LDAP because it is not new.'));
		}
		if (!$this->beforeSave()) {
			return false;
		}
		$args = $this->getAttributes($attributes);
		if (!isset($args['name'])) {
			return false;
		}
		$result = LdapUtils::groupAdd($args['name']);
		if ($result !== true) {
			return false;
		}
		$this->setIsNewRecord(false);
		$this->setScenario('update');
		$this->afterSave();
		return true;
	}

	public function update(array $attributes = null)
	{
		throw new CHttpException(405, "Groups can not be updated");
	}

	public function delete()
	{
		if ($this->getIsNewRecord()) {
			throw new CDbException(Yii::t('yii', 'Group cannot be deleted from LDAP because it is not new.'));
		}
		$args = array('group' => $this->name);
		$result = LdapUtils::groupRemove($args);
		if ($result !== true) {
			return false;
		}
		return true;
	}

	public function getIsNewRecord()
	{
		return $this->_new;
	}

	public function setIsNewRecord($value)
	{
		$this->_new = $value;
	}
}