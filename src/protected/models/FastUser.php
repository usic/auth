<?php

/**
 *   FastUser - model to store user data in mongodb.
 *   Fast - becouse it provides much faster acsses to user data than LDAP based model.
 *
 */
class FastUser extends EMongoDocument
{

	/* Values from icc */
	public $surName; //Entered by user on registretion too check with icc values
	public $firstName;
	public $middleName;
	public $faculty;
	public $profession;
	public $degree; //Curent education level
	public $pasCode;
	public $year;

	/* User entered values */
	public $login;
	public $email;
	public $phone;

	/* Programm generated values */
	public $confirmationCode;

	/** @virtual */
	public $newEmail;
	/** @virtual */
	public $newPhone;

	/* User`s groups */
	public $groups = array();


	/** @virtual */
	private static $_slowToFast = array(
		'login'      => 'login',
		'firstName'  => 'name',
		'surName'    => 'surname',
		'middleName' => 'middleName',
		'profession' => 'profession',
		'degree'     => 'level',
		'email'      => 'email'
	);

	public function collectionName()
	{
		return 'users';
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(

			array('password,newPassword', 'required', 'on' => 'changePassword'),

			array('email,newEmail', 'required', 'on' => 'changeEmail'),
			array('email', 'email', 'checkMX' => true, 'on' => 'changeEmail'),

			array('newPhone', 'required', 'on' => 'changePhone'),

			array('phone', 'match', 'pattern' => '/^0?(39|50|63|66|67|68|91|92|93|94|95|96|97|98|99)+?\d{7}$/'),

			array('surName, firstName, middleName, faculty, profession, degree, pasCode, year, login, email, phone', 'safe', 'on' => 'test'),

			array('surName, firstName, middleName, faculty, profession, degree, pasCode, year, login, email, phone', 'safe', 'on' => 'admin')
		);
	}

	public function attributeNames()
	{
		return array('surName', 'firstName', 'middleName', 'faculty', 'profession', 'degree',
			'year', 'login', 'email', 'phone');
	}

	public function attributeLabels()
	{
		return array(
			'login'        => 'Логін',
			'password'     => 'Пароль',
			'firstName'    => 'Ім\'я',
			'surName'      => 'Прізвище',
			'middleName'   => 'По-батькові',
			'profession'   => 'Спеціальність',
			'degree'       => 'Рівень',
			'email'        => 'Email',
			'phone'        => 'Телефон',
			'enteringYear' => 'Рік вступу',
			'pasCode'      => 'Номер паспорта'
		);
	}

	/**
	 *   Returns user`s full name
	 * @return string full name
	 *
	 */
	public function getFullName()
	{
		return $this->surName . " " . $this->firstName . " " . $this->middleName;
	}

	public static function findByLogin($login)
	{
		return FastUser::model()->findOne(array("login" => $login));
	}

	/**
	 * Syncs FastUser with SlowUser
	 * @param  array $attributes which attributes should be synced
	 * @param  string $password password to be saved
	 * @return boolean
	 */
	public function syncToSlowModel($attributes = null, $password = null)
	{
		if (isset($this->login)) {
			$slowUser = SlowUser::model()->findById($this->login);
			if ($slowUser === false && !is_null($password)) {
				$slowUser = new SlowUser;
			} elseif ($slowUser === false) {
				return false;
			}
		} else {
			return false;
		}
		if (is_null($attributes)) {
			$toSync = self::$_slowToFast;
		} else {
			$toSync = array();
			foreach ($attributes as $attribute) {
				if (array_key_exists($attribute, self::$_slowToFast)) {
					$toSync[$attribute] = self::$_slowToFast[$attribute];
				}
			}
		}
		if (!is_null($password)) {
			$slowUser->password = $password;
		}
		foreach ($toSync as $fastAttribute => $slowAttribute) {
			$slowUser->$slowAttribute = $this->$fastAttribute;
		}
		return $slowUser->save();
	}

	/**
	 * Generate confirmation code
	 * @param string $code
	 * @return string confirmation code
	 */
	public function generateConfirmationCode($code = null)
	{
		if(is_null($code)) {
			$hex = bin2hex(openssl_random_pseudo_bytes(16));
			$this->confirmationCode = sha1($hex . $this->login . $this->email . Yii::app()->name);
		} else {
			$this->confirmationCode = $code;
		}
		$this->save();
		return $this->confirmationCode;
	}

	/**
	 * Function to confirm user
	 * @param  string $code confirmation code
	 * @return boolean       User confirmed/not
	 */
	public function confirm($code)
	{
		if ($this->checkCode($code) && !in_array('confirmed', $this->groups)) {
			//Add user to group
			return $this->addToGroup('confirmed');
		} else if (in_array('confirmed', $this->groups)){
			//User is already confirmed
			return true;
		} else {
			//Incorrect code
			return false;
		}
	}

	public function checkCode($code)
	{
		//Each code can be used only once
		if($code != "0" && $this->confirmationCode === $code) {
			$this->generateConfirmationCode(0);
			return true;
		} else {
			$this->generateConfirmationCode(0);
			return false;
		}
	}

	/**
	 * Add User to group. First checks if user added in LDAP group, then add group name to group array
	 * @param  string $group Group name
	 * @return boolean          added/not
	 */
	public function addToGroup($group)
	{
		if (LdapUtils::groupAddUser(array('login' => $this->login, 'group' => $group))) {
			if(!in_array($group, $this->groups)){
				array_push($this->groups, $group);
				$this->save();
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Remove User from group. First checks if user removed from LDAP group, then remove group name from group array
	 * @param  string $group Group name
	 * @return boolean       removed/not
	 */
	public function removeFromGroup($group)
	{
		if (LdapUtils::groupRemove(array('login' => $this->login, 'group' => $group))) {
			if(in_array($group, $this->groups)){
				$this->groups = array_diff($this->groups, array($group));
				$this->save();
			}
			return true;
		} else {
			return false;
		}
	}

	protected function beforeValidate()
	{
		$this->phone = str_replace(array('+38', '-', ' ', '(', ')'), '', $this->phone);
		return parent::beforeValidate();
	}

}