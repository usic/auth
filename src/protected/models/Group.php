<?php

class Group extends LdapGroup
{

	public $name;

	public function __construct()
	{
		$this->setIsNewRecord(true);
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function rules()
	{
		return array(
			array('name', 'match', 'pattern' => '/^([a-z0-9_-]{3,20})$/'),
		);
	}

	public function attributeNames()
	{
		return array('name');
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Ім\'я групи',
		);
	}

	public static function stats($groups)
	{
		$data = Yii::app()->mongodb->users->aggregate(
			array('$project' => array(
				'groups' => 1
			)),
			array('$unwind' => '$groups'),
			array('$match' => array(
				'groups' => array('$in' => $groups)
			)),
			array('$group' => array(
				'_id' => '$groups',
				'users' => array('$sum' => 1)
			)),
			array('$project' => array(
				'_id'   => 0,
				'group' => '$_id',
				'users' => '$users'
			))
		);
		if($data['ok'] == 1) {
			return $data['result'];
		} else {
			throw new CDbException("Can't aggregate groups data");
		}
	}
}