<?php
/**
 * @docs http://www.w3schools.com/tags/tag_meta.asp
 */

$socials = array('social_description', 'social_images', 'social_title');
$w3c = array('application-name', 'author', 'description', 'generator', 'keywords');

$data = isset($data) ? $data : array();
$metadata = (isset(Yii::app()->params['meta']) && is_array(Yii::app()->params['meta']))
	? array_merge_recursive(Yii::app()->params['meta'], $data) : $data;

foreach ($metadata as $key => $value) {
	if (in_array($key, $w3c)) {
		Yii::app()->clientScript->registerMetaTag($value, $key);
	}
	if (in_array($key, $socials)) {
		$key = str_replace('social_', '', $key);
		if ($key == 'images') {
			$images = is_array($value) ? $value : array($value);
			foreach ($images as $image)
				Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl($image), null, null, array('property' => 'og:image'));
		} else {
			Yii::app()->clientScript->registerMetaTag($value, null, null, array('property' => 'og:' . $key));
		}
	}
}