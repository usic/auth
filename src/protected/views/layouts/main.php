<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="/images/icons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/images/icons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/images/icons/favicon-16x16.png" sizes="16x16">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<link href='https://fonts.googleapis.com/css?family=Lato:700' rel='stylesheet' type='text/css'>
</head>
<body class="layout bg-grey">
<?php echo $content; ?>
<?php if (isset(Yii::app()->params['yandexMetrika'])): ?>
	<?php Yii::app()->clientScript->registerScript('yandex metrika', Yii::app()->params['yandexMetrika']); ?>
<?php endif; ?>
</body>
</html>

