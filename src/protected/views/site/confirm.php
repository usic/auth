<?php

$this->pageTitle = Yii::app()->name . " - Confirm"

?>

<div class="auth-container bg-white border-1 box autocenter padding-base shadow">
	<header>
		<hgroup class="usic-title">
			<div class="two-collumn bottom box">
				<div class="logo bg-usic"></div>
			</div>
			<h2 class="usic-text align-left font-lato font-60 bold cl-usic caps two-collumn bottom box">usic</h2>

			<h3 class="title-description font-18 align-center grid-top">Студентський інтернет центр НаУКМА</h3>
		</hgroup>
		<hr class="line bg-line grid-top grid-vertical">
	</header>

	<div class="form">
		<h3 class="title-description font-18 align-center grid-top"><?php echo CHtml::encode($message) ?></h3>
	</div>

	<hr class="line bg-line grid-top grid-vertical">
	<div class="link font-18 align-center">
		<a href="http://feedback.usic.at/">Зворотній зв'язок</a>
	</div>
</div>