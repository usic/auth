<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Вхід';
$this->renderPartial('//layouts/meta');
?>
<div class="auth-container bg-white border-1 box autocenter padding-base shadow">
	<header>
		<hgroup class="usic-title">
			<div class="two-collumn bottom box">
				<div class="logo bg-usic"></div>
			</div>
			<h2 class="usic-text align-left font-lato font-60 bold cl-usic caps two-collumn bottom box">usic</h2>

			<h3 class="title-description font-18 align-center grid-top">Студентський інтернет центр НаУКМА</h3>
		</hgroup>
		<hr class="line bg-line grid-top grid-vertical">
		<hgroup class="service-title">
			<h1 class="align-center font-18 cl-usic">
				<?php if (Yii::app()->user->hasFlash('message')) {
					echo Yii::app()->user->getFlash('message');
				} else {
					echo "Авторизація в сервісах центру";
				} ?>
			</h1>
		</hgroup>
	</header>

	<div class="form">

		<?php $form = $this->beginWidget('CActiveForm', array(
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			'htmlOptions'   => array(
				'name'  => 'authorization',
				'class' => 'auth-form'
			)

		)); ?>

		<?php echo $form->error($model, 'username', array('class' => 'errorMessage align-center')); ?>
		<?php echo $form->error($model, 'password', array('class' => 'errorMessage align-center')); ?>

		<?php echo $form->textField($model, 'username',
			array(
				'class'        => 'input font-22 grid-vertical box round',
				'placeholder'  => $model->getAttributeLabel('username'),
				'autocomplete' => 'off',
				'autofocus'    => 'true',
				'required'     => 'true',
				'oninvalid'    => 'setCustomValidity(\'Введіть логін!\')',
				'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}')
		); ?>


		<?php echo $form->passwordField($model, 'password', array(
				'class'        => 'input font-22 box round',
				'placeholder'  => $model->getAttributeLabel('password'),
				'autocomplete' => 'off',
				'required'     => 'true',
				'oninvalid'    => 'setCustomValidity(\'Введіть пароль!\')',
				'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}')
		); ?>

		<div class="grid-top">
			<span class="two-collumn middle align-left box another-computer">
				<?php echo $form->checkBox($model, 'doNotRememberMe', array('class' => 'checkbox font-24 round', 'id' => 'computer')); ?>
				<label for="computer" class="label font-18 cl-grey"
				       title="Сесія завершиться після закриття браузера"><?php echo $model->getAttributeLabel('doNotRememberMe'); ?></label>
			</span>

			<span class="two-collumn middle box submit-block">
				<?php echo CHtml::submitButton('Увійти', array('class' => 'button submit round font-24 bg-usic cl-white', 'id' => 'login')); ?>
			</span>

			<hr class="line bg-line grid-top grid-vertical">
			<div class="link font-18 inline-block">
				<?php echo CHtml::link('Відновити пароль', array('/site/passwordreset')); ?>
			</div>

			<div class="link font-18 inline-block right">
				<a href="http://feedback.usic.at/">Зворотній зв'язок</a>
			</div>
		</div>

		<?php $this->endWidget(); ?>
	</div>
	<!-- form -->
	<div class="registration font-24">
		<?php echo CHtml::link('Зареєструватись', array('/site/registration')); ?>
	</div>
</div>
<?php

Yii::app()->clientScript->registerScript('hideMessage',"$('.message').click(function(){
              $(this).animate({top: -$(this).outerHeight()}, 500);
      });");
?>
