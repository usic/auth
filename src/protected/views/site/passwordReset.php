<?php
/* @var $this SiteController */

/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name.' - Відновити пароль';
?>
<div class="auth-container bg-white border-1 box autocenter padding-base shadow">
	<header>
		<hgroup class="usic-title">
			<div class="two-collumn bottom box">
				<div class="logo bg-usic"></div>
			</div>
			<h2 class="usic-text align-left font-lato font-60 bold cl-usic caps two-collumn bottom box">usic</h2>

			<h3 class="title-description font-18 align-center grid-top">Студентський інтернет центр НаУКМА</h3>
		</hgroup>
		<hr class="line bg-line grid-top grid-vertical">
		<hgroup class="service-title">
			<h1 class="align-center font-18 cl-usic">
				<?php
				if (Yii::app()->user->hasFlash('message')) {
					echo Yii::app()->user->getFlash('message');
				}
				?>
			</h1>
		</hgroup>
	</header>

	<div class="form">

		<?php $form = $this->beginWidget('CActiveForm', array(
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			'htmlOptions'   => array(
				'name'  => 'authorization',
				'class' => 'auth-form'
			)

		)); ?>

		<?php echo $form->error($model, 'password'); ?>
		<?php echo $form->error($model, 'repeatPassword'); ?>

		<?php echo $form->passwordField($model, 'password',
			array(
				'class'        => 'input font-22 grid-vertical box round',
				'placeholder'  => $model->getAttributeLabel('password'),
				'autocomplete' => 'off',
				'autofocus'    => 'true',
				'required'     => 'true',
				'oninvalid'    => 'setCustomValidity(\'Введіть логін!\')',
				'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}')
		); ?>


		<?php echo $form->passwordField($model, 'repeatPassword', array(
				'class'        => 'input font-22 box round',
				'placeholder'  => $model->getAttributeLabel('repeatPassword'),
				'autocomplete' => 'off',
				'required'     => 'true',
				'oninvalid'    => 'setCustomValidity(\'Введіть пароль!\')',
				'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}')
		); ?>

		<div class="grid-top align-center">
            <span class="middle box submit-block">
				<?php echo CHtml::submitButton('Змінити', array('class' => 'button submit round font-24 bg-usic cl-white')); ?>
			</span>
		</div>

		<?php $this->endWidget(); ?>
	</div>
	<!-- form -->


</div>