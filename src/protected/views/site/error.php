<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Упс, сталась помилка';
?>
<div class="reg-container bg-white border-1 box automargin align-center padding-base shadow">
	<h2 class="cl-usic font-60"><?php echo $code; ?></h2>
	<?php if ($code == 404): ?>
		<h2 class="font-24">Сторінку не знайдено</h2>
		<hr class="line bg-line grid-top grid-vertical">
		<h3 class="font-18">Нам дуже шкода, але така сторінка не існує.</h3>
	<?php elseif ($code == 403): ?>
		<h2 class="font-24">Доступ заборонено</h2>
		<hr class="line bg-line grid-top grid-vertical">
		<h3 class="font-18">Доступ до даної ділянки сайту закрито.</h3>
	<?php else: ?>
		<h3 class="font-24">Вибачте, але сталася помилка. Ми вже працюємо над її виправленням.</h3>
		<hr class="line bg-line grid-top grid-vertical">
		<div class="grid-top font-22 link">
			<a href="http://feedback.usic.at/">Зворотній зв'язок</a>
		</div>
	<?php endif; ?>
	<a href="https://my.usic.at/">
		<div class="error404logo blue_logo bg-usic"></div>
	</a>
</div>