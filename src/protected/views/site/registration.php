<?php
/* @var $this SiteController */
/* @var $model RegistrationForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Реєстрація';

$this->renderPartial('//layouts/meta');
?>
<div class="reg-container bg-white border-1 box automargin padding-base shadow">
	<header>
		<hgroup class="usic-title">
			<div class="two-collumn bottom box">
				<div class="logo bg-usic"></div>
			</div>
			<a href="/"><h2 class="usic-text align-left font-lato font-60 bold cl-usic caps two-collumn bottom box">
					usic</h2></a>

			<h3 class="title-description font-18 align-center grid-top">Студентський інтернет центр НаУКМА</h3>
		</hgroup>
		<hr class="line bg-line grid-top grid-vertical">
		<hgroup class="service-title">
			<h1 class="align-center font-18 cl-usic">Реєстрація в сервісах центру</h1>
		</hgroup>
	</header>

	<div class="form">
		<div class="help align-center border-1 round inline-block">
			<h3 class="cl-usic"><b>Підказка:</b></h3>
			<span id="help"></span>
			<div class="link">
				<a href="http://feedback.usic.at/">Зворотній зв'язок</a>
			</div>
		</div>
		<div class="input-wrapper inline-block">
			<?php $form = $this->beginWidget('CActiveForm', array(
				'clientOptions' => array(
					'validateOnSubmit' => true,
				),
				'htmlOptions'   => array(
					'name'  => 'registration',
					'class' => 'auth-form'
				)
			)); ?>

			<?php echo $form->errorSummary($model, '','',array('class'=>'errorMessage')); ?>

			<?php echo $form->textField($model, 'login',
				array(
					'class'        => 'input font-22 grid-vertical box round',
					'placeholder'  => $model->getAttributeLabel('login'),
					'autocomplete' => 'off',
					'autofocus'    => 'true',
					'required'     => 'true',
					'oninvalid'    => 'setCustomValidity(\'Введіть логін!\')',
					'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}',
					'onfocus'      => 'document.getElementById("help").innerHTML = "Латинські букви, цифри. 5 - 20 символів"'
				)
			); ?>

			<?php echo $form->passwordField($model, 'password', array(
					'class'        => 'input font-22 box round grid-bottom',
					'placeholder'  => $model->getAttributeLabel('password'),
					'autocomplete' => 'off',
					'required'     => 'true',
					'oninvalid'    => 'setCustomValidity(\'Введіть пароль!\')',
					'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}',
					'onfocus'      => 'document.getElementById("help").innerHTML = "Не менше 6 символів"'
				)
			); ?>

			<?php echo $form->emailField($model, 'email',
				array(
					'class'        => 'input font-22 box round grid-bottom',
					'placeholder'  => $model->getAttributeLabel('email'),
					'autocomplete' => 'off',
					'required'     => 'true',
					'oninvalid'    => 'setCustomValidity(\'Введіть e-mail!\')',
					'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}',
					'onfocus'      => 'document.getElementById("help").innerHTML = "Email, на який Вам прийде лист з підтвердженням реєстрації"'
				)
			); ?>

			<?php echo $form->textField($model, 'surName',
				array(
					'class'        => 'input font-22 box round grid-bottom',
					'placeholder'  => $model->getAttributeLabel('surName'),
					'autocomplete' => 'off',
					'required'     => 'true',
					'oninvalid'    => 'setCustomValidity(\'Введіть прізвище!\')',
					'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}',
					'onfocus'      => 'document.getElementById("help").innerHTML = "Ваше прізвище кирилицею. Використовується для ідентифікації Вас як студента НаУКМА"'
				)
			); ?>

			<?php echo $form->textField($model, 'pasCode',
				array(
					'class'        => 'input font-22 box round',
					'placeholder'  => $model->getAttributeLabel('pasCode'),
					'autocomplete' => 'off',
					'required'     => 'true',
					'oninvalid'    => 'setCustomValidity(\'Введіть код перевірки!\')',
					'onchange'     => 'try{setCustomValidity(\'\')}catch(e){}',
					'onfocus'      => 'document.getElementById("help").innerHTML = "Друга буква серії та останні 3 цифри номера Вашого паспорта. Використовується для ідентифікації Вас як студента НаУКМА"'
				)
			); ?>
		</div>
		<div class="grid-top align-center">
            <span class="middle box submit-block">
                <?php echo CHtml::submitButton('Зареєструватися', array('class' => 'button submit round font-24 bg-usic cl-white')); ?>
            </span>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>