<div class="my-container bg-white border-1 box automargin  padding-base shadow">
	<?php $this->renderPartial('//user/partials/_profileHeader'); ?>
	<div class="services_wrapper">
		<h1><?php echo CHtml::encode($group) ?></h1>

		<?php
		$this->widget('zii.widgets.grid.CGridView', array(
			'dataProvider' => $dataProvider,
			'columns' => array(
				'login:html',
				'fullName:html',
				array(
					'class' => 'CButtonColumn',
					'updateButtonUrl' => 'Yii::app()->createAbsoluteUrl("admin/userUpdate", array("id" => (string) $data->_id))',
					'template' => '{update}'
				)
			)
		));

		?>
	</div>
</div>
 