<div class="my-container bg-white border-1 box automargin  padding-base shadow">
	<?php $this->renderPartial('//user/partials/_profileHeader'); ?>
	<div class="services_wrapper">
		<h1><?php echo CHtml::encode($model->login) ?></h1>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			'htmlOptions'   => array(
				'name'  => 'update',
				'class' => 'auth-form'
			)
		)); ?>

		<?php echo $form->errorSummary($model, '','',array('class'=>'errorMessage')); ?>

		<?php echo $form->textField($model, 'surName',
			array(
				'class'        => 'input font-22 box round grid-bottom',
				'placeholder'  => $model->getAttributeLabel('surName'),
				'autocomplete' => 'off',
			)
		); ?>

		<?php echo $form->textField($model, 'firstName',
			array(
				'class'        => 'input font-22 box round grid-bottom',
				'placeholder'  => $model->getAttributeLabel('firstName'),
				'autocomplete' => 'off',
			)
		); ?>

		<?php echo $form->textField($model, 'middleName',
			array(
				'class'        => 'input font-22 box round grid-bottom',
				'placeholder'  => $model->getAttributeLabel('middleName'),
				'autocomplete' => 'off',
			)
		); ?>

		<?php echo $form->emailField($model, 'email',
			array(
				'class'        => 'input font-22 box round grid-bottom',
				'placeholder'  => $model->getAttributeLabel('email'),
				'autocomplete' => 'off',
			)
		); ?>

		<?php echo $form->textField($model, 'pasCode',
			array(
				'class'        => 'input font-22 box round grid-bottom',
				'placeholder'  => $model->getAttributeLabel('pasCode'),
				'autocomplete' => 'off',
			)
		); ?>

	<div class="grid-top align-center">
            <span class="middle box submit-block">
                <?php echo CHtml::submitButton('Save', array('class' => 'button submit round font-24 bg-usic cl-white')); ?>
            </span>
	</div>
	<?php $this->endWidget(); ?>

		<h2>Groups:</h2>

		<ul id="groups">
		<?php foreach($model->groups as $group): ?>
			<li>
				<span><?php echo CHtml::encode($group); ?></span>
				<?php echo CHtml::ajaxButton('Remove', $this->createUrl('admin/groupRemove'),
						array(
							'type' => 'POST',
							'data' => array(
								'group' => $group,
							    'user' => (string) $model->_id,
							    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
							),
							'success' => 'js: function() {
								location.reload();
							}',
							'error' => 'js: function(q, text, error) {
								alert(error);
							}'
						)
					);
				?>
			</li>
		<?php endforeach; ?>
		</ul>

		<h2>Add to group:</h2>
		<div>
			<?php echo CHtml::beginForm(); ?>
			<?php echo CHtml::textField('group'); ?>
			<?php echo CHtml::hiddenField('user', (string) $model->_id); ?>
			<?php echo CHtml::ajaxSubmitButton('Add', $this->createUrl('admin/groupAdd'),
				array(
					'type' => 'POST',
					'success' => 'js: function() {
						location.reload();
					}',
					'error' => 'js: function(q, text, error) {
						alert(error);
					}'
				)
			); ?>
			<?php echo CHtml::endForm(); ?>
		</div>
	</div>
</div>
 