<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>

<div style="width: 1000px; margin: 0 auto;">
	<table cellpadding="0" cellspacing="0" style="border: 1px solid #cecece; border-radius: 4px; padding: 40px;"
	       width="900px">

		<tr>
			<td style="width: 106px; height: 106px;"><img src="http://usic.org.ua/~robert/static/logotype.png"></td>
			<td style="padding-left: 8px;"><span
					style="font: bold 42px Arial; color: #cecece; display: block; padding-top: 18px;">
	<span style="color: #2592ff;">U</span>kma <span style="color: #2592ff;">S</span>tudent <span
						style="color: #2592ff;">I</span>nternet <span style="color: #2592ff;">C</span>enter</span>
				<span style="font: 18px Arial; color: #cecece; display: block; padding-top: 6px;">студентський інтернет-центр НаУКМА</span>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="text-align: center; padding: 40px 0; font: 18px Arial; line-height: 32px;">
				<a href="" style="color: #2592ff;"><?php echo CHtml::encode($fullName); ?></a>
				<span style="display: block; font: 14px Arial; padding-top: 8px;">Для зміни паролю перейдіть за посиланням: </span>
				<a href="<?php echo CHtml::encode($confirmationUrl); ?>"
				   style="color: #2592ff; font: 14px Arial;"><?php echo CHtml::encode($confirmationUrl); ?></a>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="padding: 40px 0; font: 18px Arial; text-align: center; line-height: 32px;">
				Якщо у Вас виникли питання, скористайтеся формою зворотнього зв'язку: <a href="http://feedback.usic.at"
				                                                                         style="color: #2592ff;">feedback.usic.at</a>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="padding: 20px 0 40px 0; font: 36px Arial; text-align: center;">
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
