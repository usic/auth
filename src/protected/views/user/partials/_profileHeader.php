<header>
	<hgroup class="usic-title">
		<div class="bottom box inline-block">
			<div class="blue_logo"></div>
		</div>
		<hgroup class="inline-block">
			<h2 class="usic-text align-left font-lato font-42 bold cl-usic caps bottom box">usic</h2>

			<h3 class="title-description font-14 align-center inline-block">Студентський інтернет центр
				НаУКМА</h3>
		</hgroup>
	</hgroup>
	<hr class="line bg-line grid-top grid-vertical">
	<?php if (Yii::app()->user->hasFlash('message')): ?>
		<hgroup class="service-title">
			<h1 class="align-center font-18 cl-usic">
				<?php echo Yii::app()->user->getFlash('message'); ?>
			</h1>
		</hgroup>
	<?php endif; ?>
	<div class="menu font-14">
		<a href=<?php echo $this->createUrl("site/logout") ?> id="logout">
			<div class="bg-usic inline-block cl-white"><?php echo CHtml::encode(Yii::app()->user->login); ?>
				(logout)
			</div>
		</a>
	</div>
</header>
 