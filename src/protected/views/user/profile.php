<?php
	$this->pageTitle = Yii::app()->name . ' - Профіль користувача';
?>
	<div class="my-container bg-white border-1 box automargin  padding-base shadow">
		<?php $this->renderPartial('partials/_profileHeader'); ?>
		<div class="services_wrapper">
			<ul class="services">
				<li>
					<a href="http://wiki.usic.org.ua/" target="_blank">
						<div class="wiki"></div>
						<span class="cl-wiki">Usic Wiki</span></a>

					<h3 class="align-center font-14 grid-top">Підготуйся до заліку</h3>
				</li>
				<li>
					<a href="http://tt.usic.at/" target="_blank">
						<div class="tt"></div>
						<span class="cl-tt">TimeTable</span></a>

					<h3 class="align-center font-14 grid-top">Твій розклад онлайн</h3>
				</li>
				<li>
					<a href="http://fs.usic.at/" target="_blank">
						<div class="fs"></div>
						<span class="cl-fs">Fileshare</span></a>

					<h3 class="align-center font-14 grid-top">Обмінюйся файлами в академії</h3>
				</li>
			</ul>
		</div>
		<hr class="line bg-line grid-top grid-vertical">
		<footer>
			<div class="link align-center">
				<a href="http://feedback.usic.at/">Зворотній зв'язок</a>
			</div>
		</footer>
	</div>