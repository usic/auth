<?php


class UserController extends Controller
{
	public $defaultAction = 'profile';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('profile', 'settings'),
				'users'   => array('@'),
			),
			array(
				'deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * @description Shows user's profile and useful links
	 */
	public function actionProfile()
	{
		//TODO Create user view
		/*if(isset(Yii::app()->user->login)) {
			$login = Yii::app()->user->login;
		} else {
			$this->redirect('/');
		}
		$model = FastUser::findByLogin($login);*/
		$this->render('profile');
	}

	/**
	 * @description Allows user to change default settings
	 */
	public function actionSettings()
	{
		//TODO: Add possibility to change user's settings (settings array in user model)
		throw new CHttpException(501, Yii::t('app','Method Not implemented'));
	}
}