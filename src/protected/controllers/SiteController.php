<?php

/**
 * Class SiteController
 */
class SiteController extends Controller
{
	public $defaultAction = 'login';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!Yii::app()->user->isGuest) {
			$this->redirect('/user/profile');
		}
		$model = new LoginForm;
		// if it is ajax validation request
		if ((isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') || (isset($_POST['tpa']))) {
			$result = CActiveForm::validate($model);
			if (isset($_POST['tpa'])) {
				if ($result == '[]') { //without errors
					echo CJSON::encode(array('success' => "User $model->username auth successful"));
				} else {
					echo "{error:" . str_replace('LoginForm_', '', $result) . "}"; // some json
				}
				Yii::app()->end();
			}
			echo $result;
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$from = Yii::app()->request->getParam("from");
				if (self::checkReferrer()) {
					$this->redirect($from);
				}
				$this->redirect('user/profile');
			}
		}
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout($full = false)
	{
		//TODO: if full==true -> then destroy all sessions with username
		Yii::app()->user->logout();
		if ($this->checkReferrer()) {
			$this->redirect(Yii::app()->request->getParam("from"));
		}

		$this->redirect('login');
	}

	/**
	 * @throws CHttpException
	 */
	public function actionRegistration()
	{
		$model = new RegistrationForm;

		//ajax validate
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		//registration
		if (isset($_POST['RegistrationForm'])) {
			$model->attributes = $_POST['RegistrationForm'];
			if ($model->validate() && $model->saveToDB()) {

				if ($model->fastUser->syncToSlowModel(null, $model->password) === true) {
					$this->_sendMail($model->fastUser, array('subject' => Yii::t('messages', 'Підтвердження користувача'), 'view' => 'confirm', 'url' => 'site/confirm'));
					Yii::app()->user->setFlash('message', Yii::t('success', 'Вам надіслано повідомлення для підтвердження email-адреси'));
					$this->redirect('/');
				} else {
					throw new CHttpException(500, Yii::t('error', 'Сталася помилка, зверніться до адміністрації.'));
				}

			}
		}
		$this->render('registration', array('model' => $model));
	}

	public function actionConfirm($login, $code)
	{

		$user = FastUser::model()->findOne(array('login' => $login));

		if (is_null($user)) {
			$message = Yii::t('error', 'Користувач з таким логіном не існує');
		} else {
			if ($user->confirm($code)) {
				Yii::app()->user->setFlash('message', Yii::t('success', 'Ваш email підтверджено'));
				$this->redirect('user/profile');
			} else {
				$message = Yii::t('error', 'Не коректний логін або код підтвердження');
			}
		}

		$this->render('confirm', array('message' => $message));
	}

	public function actionPasswordReset()
	{
		$model = new PasswordRestoreForm();
		if (isset($_POST['PasswordRestoreForm']['password'])) {
			//Post from password form
			$model->setScenario('enterPassword');
			$model->attributes = $_POST['PasswordRestoreForm'];
			if ($model->validate() && isset(Yii::app()->session['prLogin'])) {
				$user = SlowUser::model()->findById(Yii::app()->session['prLogin']);
				Yii::app()->session->clear();
				$user->password = $model->password;
				if ($user->save()) {
					Yii::app()->user->setFlash('message', Yii::t('success', 'Пароль успішно змінено'));
					$this->redirect('/');
				} else {
					$this->render('confirm', array('message' => Yii::t('error', 'Сталася помилка. Зверніться до адміністрації')));
				}
			} else {
				$this->render('passwordReset', array('model' => $model));
			}
		} elseif (isset($_POST['PasswordRestoreForm']['email'])) {
			//Post from email form
			$model->setScenario('enterEmail');
			$model->attributes = $_POST['PasswordRestoreForm'];
			if ($model->validate()) {
				$this->_sendMail($model->fastUser, array('subject' => Yii::t('messages', 'Зміна паролю'), 'view' => 'passwordReset', 'url' => 'site/passwordReset'));
				$this->render('confirm', array('message' => Yii::t('error', 'Вам відправлений лист для зміни паролю')));
			} else {
				$this->render('emailForm', array('model' => $model));
			}
		} elseif (isset($_GET['login']) && isset($_GET['code'])) {
			//Request from mail
			$login = $_GET['login'];
			$code = $_GET['code'];
			$fastUser = FastUser::model()->findOne(array('login' => $login));
			if (!is_null($fastUser) && $fastUser->checkCode($code)) {
				Yii::app()->session['prLogin'] = $login;
				$this->render('passwordReset', array("model" => $model));
			} else {
				$this->render('confirm', array('message' => Yii::t('error', 'Не коректний логін або код підтвердження')));
			}
		} else {
			//Render view to enter mail
			$this->render('emailForm', array('model' => $model));
		}
	}

	public function actionStats()
	{
		if (!isset($_GET['groups']) || !is_array($_GET['groups'])) {
			throw new CHttpException(400);
		}
		try {
			$data = Group::stats($_GET['groups']);
			echo CJSON::encode($data);
		} catch (CDbException $e) {
			throw new CHttpException(500);
		} finally {
			Yii::app()->end();
		}
	}

	public function checkReferrer()
	{
		if (!is_null($host = Yii::app()->request->getParam("from"))) {
			foreach (Yii::app()->params['allowedHosts'] as $allowed) {
				if (strpos(
						$host,
						sprintf('http://%s%s', $allowed, Yii::app()->user->identityCookie['domain']))
					!== false
				) {
					return true;
				}
			}
		}
		return false;
	}

	private function _sendMail(FastUser $fastUser, $params)
	{
		$confirmationUrl = $this->createAbsoluteUrl($params['url'], array('login' => $fastUser->login, 'code' => $fastUser->generateConfirmationCode()));
		$mailer = Yii::app()->mail;
		$message = $mailer->newMessage(array(
			'subject' => $params['subject'],
			'from'    => Yii::app()->params['notificationEmail'],
		));
		$message->view = array($params['view'], 'fullName' => $fastUser->fullName, 'confirmationUrl' => $confirmationUrl);
		$message->to = array($fastUser->email => $fastUser->fullName);
		$mailer->send($message);
	}


}
