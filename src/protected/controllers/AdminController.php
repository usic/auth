<?php


class AdminController extends Controller
{
	public $defaultAction = 'dashboard';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('dashboard', 'group', 'userUpdate', 'userSearch', 'groupAdd', 'groupRemove'),
				'expression'   => 'Yii::app()->user->inGroup("nimda")',
			),
			array(
				'deny',
				'users' => array('*'),
			),
		);
	}

	public function actionDashboard()
	{
		$data = array();
		$groups = Group::model()->findAll();
		foreach($groups as $group) {
			$data[$group->name] = Yii::app()->mongodb->users->count(array('groups' => array('$in' => array($group->name))));
		}
		$this->render('dashboard', array('data' => $data));
	}

	public function actionGroup($id)
	{
		if(Group::model()->findById($id) === false) {
			throw new CHttpException(404, 'No such group');
		}
		$dataProvider = new EMongoDataProvider('FastUser', array(
			'criteria' => array(
				'condition' => array('groups' => array('$in' => array($id))),
				'sort' => array('login' => 1)
			)
		));
		$this->render('group', array('group' => $id, 'dataProvider' => $dataProvider));
	}

	public function actionUserUpdate($id)
	{
		$model = FastUser::model()->findBy_id($id);
		if(is_null($model)) {
			throw new CHttpException(404, 'No such user');
		}

		$model->setScenario('admin');

		if (isset($_POST['FastUser'])) {
			$model->attributes = $_POST['FastUser'];
			if ($model->validate() && $model->save()) {
				if($model->syncToSlowModel()){
					Yii::app()->user->setFlash('message', 'User updated');
				} else {
					throw new CHttpException(500, "Can not sync to LDAP");
				}
			} else {
				throw new CHttpException(500, "Can not save to mongo");
			}
		}
		$this->render('userUpdate', array('model' => $model));
	}

	public function actionUserSearch()
	{
		if(!isset($_GET['login'])) {
			throw new CHttpException(400, 'Bad request');
		}
		$login = $_GET['login'];
		$user = FastUser::findByLogin($login);
		if(is_null($user)) {
			throw new CHttpException(404);
		}

		$this->redirect($this->createUrl('admin/userUpdate', array('id' => (string) $user->_id)));
	}

	public function actionGroupAdd()
	{
		if(!Yii::app()->request->getIsAjaxRequest() && (!isset($_POST['user']) || !isset($_POST['group']))) {
			throw new CHttpException(400, 'Not ajax request');
		}
		$user = $_POST['user'];
		$group = $_POST['group'];
		header('Content-Type: ' . 'application/json');
		$user = FastUser::model()->findBy_id($user);
		if(is_null($user)) {
			header('HTTP/1.1 404 No such User');
			echo CJSON::encode(array('success' => false, 'error' => 'No such user'));
		} else {
			if($user->addToGroup($group)) {
				echo CJSON::encode(array('success' => true, 'error' => 'none'));
			} else {
				header('HTTP/1.1 500 Can not add user to group');
				echo CJSON::encode(array('success' => false, 'error' => 'Can not add user to group'));
			}
		}
		Yii::app()->end();
	}

	public function actionGroupRemove()
	{
		if(!Yii::app()->request->getIsAjaxRequest() && (!isset($_POST['user']) || !isset($_POST['group']))) {
			throw new CHttpException(400, 'Not ajax request');
		}
		$user = $_POST['user'];
		$group = $_POST['group'];
		header('Content-Type: ' . 'application/json');
		$user = FastUser::model()->findBy_id($user);
		if(is_null($user)) {
			header('HTTP/1.1 404 No such User');
			echo CJSON::encode(array('success' => false, 'error' => 'No such user'));
		} else {
			if($user->removeFromGroup($group)) {
				echo CJSON::encode(array('success' => true, 'error' => 'none'));
			} else {
				header('HTTP/1.1 500 Can not remove user from group');
				echo CJSON::encode(array('success' => false, 'error' => 'Can not remove user from group'));
			}
		}
		Yii::app()->end();
	}
}