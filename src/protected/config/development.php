<?php
return array(
	'components'=>array(
		'mail' => array(
			'class' => 'DummyMailer',
			'behaviors' => array(
				'CopyToFilesBehavior',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'application.lib.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters' => array('*'),
					'levels' => 'error, warning, info, trace',
				),
			),
		),
	),
);