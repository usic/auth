<?php

return array(
	'id'                => 'usic',
	'basePath'          => realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'),
	'name'              => 'Usic Accounts',
	'defaultController' => 'site/login',
	// preloading 'log' component
	'preload'           => array('log'),

	// autoloading model and component classes
	'import'            => array(
		'application.models.*',
		'application.components.*',
		'application.lib.sammaye.mongoyii.*',
		'application.lib.mail.*',
		'application.lib.crisu83.yii-extension.behaviors.*'
	),

	// application components
	'components'        => array(
		'mail' => array(
			'class' => 'DummyMailer',
		),
		'mongodb'      => array(
			'class'  => 'EMongoClient',
			'server' => 'mongodb://localhost:27017',
			'db'     => 'accounts'
		),
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin'  => true,
			'class'           => 'UsicUser',
			'autoUpdateFlash' => false,
		),
		'redisCache'   => array(
			'class'      => 'application.lib.redis.CRedisCache',
			'predisPath' => 'application.lib.redis.Predis',
			'hashKey'    => false,
			'servers'    => array(
				array(
					'database' => 0,
					'host'     => 'localhost',
					'port'     => 6379,
				),
			),
		),
		'session'      => array(
			'class'        => 'UsicCCacheHttpSession',
			'cacheID'      => 'redisCache',
			'autoStart'    => true,
			'cookieMode'   => 'allow',
			'cookieParams' =>
				array(
					'path'     => '/',
					'domain'   => ((PHP_SAPI != 'cli') ? substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], '.')) : false),
					'httpOnly' => true,
				)
		),

		'urlManager'   => array(
			'urlFormat'      => 'path',
			'showScriptName' => true,
			'rules'          => array(
				'<action:(login|logout|registration|confirm|passwordreset)>' => 'site/<action>',
				'<action:(profile)>'                                         => 'user/<action>',
				'<controller:\w+>/<action:\w+>/<id:\w+>'                     => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'                              => '<controller>/<action>',
			),
		),
		'request'      => array(
			'enableCsrfValidation' => true,
			'csrfTokenName'        => 'YII_CSRF_TOKEN',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	'params'            => array(
		'adminEmail'        => 'webmaster@usic.at',
		'notificationEmail' => array('no-reply@usic.at' => 'USIC'),
		'allowedHosts'      => array(
			'feedback',
			'catalog',
			'cat',
			'tt',
			'mark',
			'fs'
		),
		// see /views/layouts/meta for details
		'meta'              => array(
			'keywords'           => 'usic,наукма, юізк, студентський інтернет центр, реєстрація юзік,юзік, вікі юзік, usic, naukma, зареєструватись в юзіку, стажування наукма, могилянка',
			'description'        => 'Реєстрація в сервісах студентського інтернет центру НаУКМА. Зареєструйся один раз і користуйся усіма перевагами одразу!',
			'social_description' => 'Реєстрація в сервісах студентського інтернет центру НаУКМА. Зареєструйся один раз і користуйся усіма перевагами одразу!',
			'social_title'       => 'Реєстрація в USIC. Зареєструйся один раз і користуйся усіма сервісами одразу!',
			'social_images'      => '/images/logo.png'
		)
	),
);
