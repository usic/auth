<?php

return array(
	'components' => array(
		'mongodb'    => array(
			'server' => 'mongodb://staging-mongo.usic.lan/catalog',
		),
		'redisCache' => array(
			'servers' => array(
				array(
					'host' => 'staging-redis.usic.lan',
					'password' => 'password'
				),
			),
		),
		'log' => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'     => 'application.lib.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
					'ipFilters' => array('*'),
					'levels'    => 'error, warning, info, trace',
				),
			),
		),
	),
);