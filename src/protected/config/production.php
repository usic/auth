<?php
return array(
	'components' => array(
		'mail'         => array(
			'class'     => 'Mailer',
			'transport' => array(
				'class' => 'Swift_MailTransport',
			),
		),
		'mongodb'      => array(
			'server'  => 'mongodb://MONGODBHOST',
			'options' => array('db' => 'MONGODBNAME', 'username' => 'MONGODBUSER', 'password' => 'MONGODBPASSWORD'),
			'db'      => 'MONGODBNAME'
		),
		'redisCache'   => array(
			'servers' => array(
				array(
					'host'     => 'REDISHOST',
					'password' => 'REDISPASSWORD'
				),
			),
		),
		'sentry'       => array(
			'class'             => 'application.lib.crisu83.yii-sentry.components.SentryClient',
			'dns'               => 'SENTRYDNS',
			'environment'       => 'production',
			'yiiExtensionAlias' => 'application.lib',
			'dependencies'      => array(
				'raven'         => 'application.lib.raven.raven',
				'yii-extension' => 'application.lib.crisu83.yii-extension',
			)
		),
		'urlManager'   => array(
			'showScriptName' => false,
		),
		'errorHandler' => array(
			'class' => 'application.lib.crisu83.yii-sentry.components.SentryErrorHandler',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'application.lib.crisu83.yii-sentry.components.SentryLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
	'params'     => array(
		'adminEmail'    => 'root@usic.at',
		'yandexMetrika' => '(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter23701783 = new Ya.Metrika({id:23701783, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");',
		'allowedHosts'  => array(
			'feedback',
			'fs'
		)
	)
);