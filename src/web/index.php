<?php

date_default_timezone_set('UTC');
$yii = dirname(__FILE__) . '/../protected/lib/yiisoft/yii/framework/yii.php';

$devHosts = array('localhost', '127.0.0.1', 'my.usic.at.local', '0.0.0.0');
$stagingHosts = array('my.staging.usic.at');

define('DS', DIRECTORY_SEPARATOR);
$dir = dirname(__FILE__) . DS . '..' . DS . 'protected' . DS . 'config' . DS;

$shared = require $dir . 'shared.php';
if (basename(__FILE__, '.php') == 'bootstrap') {
	$env = require $dir . 'test.php';
} elseif (php_sapi_name() == 'cli') {
	$env = require $dir . 'console.php';
} elseif (in_array($_SERVER['HTTP_HOST'], $devHosts) || in_array($_SERVER['SERVER_NAME'], $devHosts)) {
	define('YII_DEBUG', true);
	define('YII_TRACE_LEVEL', 3);
	$env = require $dir . 'development.php';
} elseif (in_array($_SERVER['HTTP_HOST'], $stagingHosts) || in_array($_SERVER['SERVER_NAME'], $stagingHosts)) {
	define('YII_DEBUG', true);
	define('YII_TRACE_LEVEL', 3);
	$env = require $dir . 'staging.php';
} else {
	$env = require $dir . 'production.php';
	$yii = dirname(__FILE__) . '/../protected/lib/yiisoft/yii/framework/yiilite.php';
}

$config = array_replace_recursive($shared, $env);

require_once($yii);
Yii::createWebApplication($config)->run();
