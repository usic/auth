<?php
// This is global bootstrap for autoloading

$yiit = dirname(__FILE__) . '/../src/protected/lib/yiisoft/yii/framework/yiit.php';

$shared = require dirname(__FILE__) . '/../src/protected/config/shared.php';
$env = require dirname(__FILE__) . '/../src/protected/config/test.php';

$config = array_replace_recursive($shared, $env);
require dirname(__FILE__) . '/_data/YiiBridge/yiit.php';

require_once($yiit);
Yii::createWebApplication($config);

