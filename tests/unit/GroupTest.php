<?php
use Codeception\Util\Stub;

class GroupTest extends \Codeception\TestCase\Test
{
	protected static $_groupName;
	protected $_group;

	protected function setUp()
	{
		parent::setUp();
		$this->_group = new Group();
	}

	public static function setUpBeforeClass()
	{
		self::$_groupName = substr(md5(time()), 0, 8);
	}

	public function testGroupValidateName()
	{
		$validGroups = array('www', 'vpn', 'nimda', 'web-course', 'some-new-group');
		foreach ($validGroups as $group) {
			$this->_group->name = $group;
			$this->assertTrue($this->_group->validate(array('name')));
		}
		$notValidGroups = array(
			'to_long_email_this_is_too_long', 'camelIsNotAllowed', 'this%too', 'and@this', 'any&'
		);
		foreach ($notValidGroups as $group) {
			$this->_group->name = $group;
			$this->assertFalse($this->_group->validate(array('name')));
		}
	}

	public function testGroupAdd()
	{
		$this->_group->name = self::$_groupName;
		$this->_group->save();
		$group = Group::model()->findById(self::$_groupName);
		$this->assertEquals($group->name, self::$_groupName);
	}

	public function testGroupRemove()
	{
		$group = Group::model()->findById(self::$_groupName);
		$this->assertTrue($group->delete());
		$this->assertFalse(Group::model()->findById(self::$_groupName));
	}
}