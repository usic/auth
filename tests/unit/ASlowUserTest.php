<?php
use Codeception\Util\Stub;

class ASlowUserTest extends \Codeception\TestCase\Test
{
	protected static $_login;
	protected static $_group;
	protected $_user;
	protected $_data
		= array(
			'student' => array(
				'login'        => 'dzuba',
				'password'     => 'alleria',
				'name'         => 'Іванов',
				'surname'      => 'Петро',
				'middleName'   => 'Сидор`ович',
				'email'        => 'test@usic.at',
				'profession'   => '42100',
				'enteringYear' => '2011',
				'level'        => SlowUser::LEVEL_BACHELOR
			),
			'teacher' => array(
				'login'      => 'opanas',
				'password'   => 'alleria',
				'name'       => 'Опанас',
				'surname'    => 'Опанасюк',
				'middleName' => 'Опанасович',
				'level'      => SlowUser::LEVEL_TEACHER
			)
		);

	protected function setUp()
	{
		parent::setUp();
		$this->_user = new SlowUser();
	}

	public static function setUpBeforeClass()
	{
		self::$_login = substr(md5(time()), 0, 8);
		self::$_group = 'test';
	}

	public function testSaveUser()
	{
		$this->_user->setScenario('test');
		$this->_user->attributes = $this->_data['student'];
		$this->_user->login = self::$_login;
		$this->assertTrue($this->_user->save());
	}


	public function testCheckUserExists()
	{
		$user = SlowUser::model()->findById(self::$_login);
		$this->assertEquals($user->login, self::$_login);
	}

	public function testUserAddToGroup()
	{
		$user = SlowUser::model()->findById(self::$_login);
		$user->addToGroup(self::$_group);
		$groups = $user->getGroups();
		$this->assertTrue(in_array(self::$_group, $groups));
	}

	public function testUserRemoveFromGroup()
	{
		$user = SlowUser::model()->findById(self::$_login);
		$user->removeFromGroup(self::$_group);
		$groups = $user->getGroups();
		$this->assertFalse(in_array(self::$_group, $groups));
	}

	public function testDeleteUser()
	{
		$user = SlowUser::model()->findById(self::$_login);
		$this->assertTrue($user->fullDelete());
	}
}
