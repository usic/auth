<?php
use Codeception\Util\Stub;


class FastUserTest extends \Codeception\TestCase\Test
{
	private $_user;
	private static $_data = array(
		'surName'    => '',
		'firstName'  => 'Петрушка',
		'middleName' => 'Петрушкович',
		'faculty'    => 'Факультет гуманітарних наук',
		'profession' => 'Культурологія',
		'degree'     => 'Бакалавр',
		'pasCode'    => 'Я333',
		'year'       => '3',
		'login'      => '',
		'email'      => 'test@test.test',
		'phone'      => '0501112233'
	);

	public static function setUpBeforeClass()
	{
		self::$_data['surName'] = substr(md5(time()), 0, 8);
		self::$_data['login'] = substr(md5(time()), 0, 8);
	}

	public static function tearDownAfterClass()
	{
		$user = SlowUser::model()->findById(self::$_data['login']);
		if ($user !== false) {
			$user->fullDelete();
		}
	}

	protected function setUp()
	{
		parent::setUp();
		$this->_user = FastUser::model()->findOne(array('login' => self::$_data['login']));
		if (is_null($this->_user)) {
			$this->_user = new FastUser();
			$this->_user->setScenario('test');
			$this->_user->attributes = self::$_data;
		}
	}

	public function testSaveUser()
	{
		$this->assertTrue($this->_user->save());
	}

	public function testFindUser()
	{
		$user = FastUser::model()->findOne(array('pasCode' => self::$_data['pasCode'], 'surName' => self::$_data['surName']));
		$this->assertAttributeEquals(self::$_data['login'], "login", $user);
	}

	public function testSyncToSlow()
	{
		//Test new user sync
		$this->assertTrue($this->_user->syncToSlowModel(null, "password"), 'New user was not synced');
		//Text exist user sync
		$this->_user->firstName = "Друговка";
		$this->assertTrue($this->_user->syncToSlowModel(), 'Old user was not synced');
		$slowUser = SlowUser::model()->findById($this->_user->login);
		$this->assertAttributeEquals($this->_user->firstName, "name", $slowUser, "Old user was synced incorrectly");
	}

	public function testAddToGroup()
	{
		$this->assertTrue($this->_user->addToGroup('test'));
	}

	public function testRemoveFromGroup()
	{
		$this->assertTrue($this->_user->removeFromGroup('test'));
	}

	public function testConfirm()
	{
		$code = $this->_user->generateConfirmationCode();
		$falseCode = md5("false code");
		//Test confirm with wrong code
		$this->assertFalse($this->_user->confirm($falseCode));
		//Test confirmation code regeneration
		$this->assertAttributeNotEquals($code, 'confirmationCode', $this->_user);
		//Test confrim with correct code
		$code = $this->_user->generateConfirmationCode();
		$this->assertTrue($this->_user->confirm($code));
		//Test previousli confirmed user with correct code
		$this->assertTrue($this->_user->confirm($code));
		//Test previousli confirmed user with wrong code
		$this->assertTrue($this->_user->confirm($falseCode));
	}

	public function testDeleteUser()
	{
		$this->assertEquals(1.0, $this->_user->delete()['ok']);
	}
}